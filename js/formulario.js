	$("#confim").click(function(){
		var error = "";
		if($("#email").val()=="")
		{
			error += " -El email es obligatorio <br>";
		}
		if($("#name_control").val()=="")
		{
			error += " -Nombre obligatorio <br>"; 
		}
		if($("#empresa_contacto").val()=="")
		{
			error += " -Nombre de su empresa obligatorio <br>"; 
		}
		if($("#contenido_contacto").val()=="")
		{
			error += " -Contenido obligatorio <br>"; 
		}
		if(error!="")
		{
			$("#alerta").html("<div class='alert alert-danger' role='alert'>"+error+"</div>");
			return false;
		}else{
			return true;
		}
	})