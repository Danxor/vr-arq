<?php
	$error = "";
	$mensaje_exito = "";

	if($_POST)
	{
		if(!$_POST['email'])
		{
			$error .= "Email obligatorio <br>";
		}
		if(!$_POST['name_control'])
		{
			$error .= "Nombre obligatorio <br>";
		}
		if(!$_POST['empresa_contacto'])
		{
			$error .= "Nombre de empresa obligatorio <br>";
		}
		if(!$_POST['contenido_contacto'])
		{
			$error .= "Contenido obligatorio <br>";
		}
		if($_POST['email'] && filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)=== false)
		{
			$error .= "Inserte una direccion de correo valida <br>";
		}
		if ($error !="") {
			$error="<div class='alert alert-danger' role='alert'><strong>Hubo un error en el formulario: <br>".$error."</strong></div>";
		}else
		{
			$para="migue.danxor@gmail.com";
			$de= $_POST['email'];
			$asunto= "Información de parte de ". $_POST['name_control'] . " de la empresa ". $_POST['empresa_contacto'];
			$contenido= $_POST['contenido_contacto'];


			include ("php/enviar.php");

		/*	if(mail("migue.danxor@gmail.com", "yo", "holi","migue_danxor@hotmail.com")){
				$mensaje_exito="<div class='alert alert-success' role='alert'>AEnviado con exito</div>";
			}else
			{
				$error="<div class='alert alert-danger' role='alert'><strong>Error en el servidor por favor intente mas tarde</strong></div>";
			}*/
		}
	}

?>


<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="jquery-3.3.1.min.js"></script>
		<script src="jquery-ui/jquery-ui.js"></script>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	    <link rel="shortcut icon" href="images/icono.png" />
		<title>Vr & Arq</title>
		<link rel="stylesheet" type="text/css" href="css/vrstyle.css">
	</head>
	<body data-spy="scroll" data-target="#navbar">
		<?php
			include 'php/barNotLog.php';
		 ?>
		<div class="jumbotron" id="intro">
			<div class="container centrado">
				<div>
		 			<?php echo $error.$mensaje_exito;?>
		 		</div>
				<div class="row">
					<div class="col-sm-4 col-xs-1">
					</div>
					<div class="col-sm-4 col-xs-10">
						<img class="img-fluid" src="images/temporal.png">
						<h1 class="display-4">Vr & Arq</h1>
						<h2 id="textoVariable" class="display-4">Soluciones Visuales</h2>
					</div>
				</div>
			</div>
		</div>

		<!-- quienes somos -->
		<div class="jumbotron quienesSomos" id="quienesSomos">
			<div class="container">
				<h1 class="display-4 centrado dfont titulo">¿Quienes somos?</h1>
				<div class="row">
					<div class=" col-lg-5">
						<video controls="" controlsList="nodownload" class="video embed-responsive embed-responsive-16by9">
							<source src="video/1.mp4" type="video/mp4">
							Tu navegador no soporta video
						</video>
					</div>
					<div class="col-lg-7">
						<p class="lead font">Somos una poderosa herramienta de previsualisacion de espacios en tiempo real. <br>
						Rompiendo todas las barreras de comunicación mostrando el espacio de tus sueños como en tus sueños.
						</p>
					</div>
				</div>
			</div>
		</div>

		<!-- que hacemos -->
		<div class="jumbotron" id="queHacemos">
			<div class="container">
				<h1 class="display-4 centrado dfont titulo">¿Qué hacemos?</h1>
				<div class="row">
					<div class="col-lg-7">
						<p class="lead font">Hacemos previsualisaciones en tiempo real para que puedas ver y recorrer los espacios
						que el dia e mañana te llenaran de alegrias. Por medio de la realidad virtual(VR) y la realida aumentada(AR)
						te ayudamos a VIVIR la casa de tus sueños aun cuando no se ha puesto la primera piedra. <br>
						Porque ver no basta VIVELO -Vr&Arq.
						</p>
					</div>
					<div class=" col-lg-5">
						<input type="image" src="images/Vr.jpg" class="img-fluid img" data-toggle="modal" data-target="#fotos">
						<small id="emailHelp" class="centrado form-text text-muted">Click para expandir galeía.</small>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="fotos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content	">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLongTitle">Galería</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				  <ol class="carousel-indicators">
				    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
				  </ol>
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <img class="d-block w-100" src="images/Vr.jpg">
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="images/q_background.jpg">
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="images/headset.jpeg" >
				    </div>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="images/temporal.png" >
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Siguiente</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Anterior</span>
				  </a>
				</div>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- vr y ar -->
		<div class="jumbotron" id="vrAr">
			<div class="container-fluid">
				<h1 class="display-4 centrado dfont titulo">¿Vr y Ar?</h1>
				<div class="row">
					<div class=" col-lg-5">
						<iframe width="800" height="450" allowfullscreen src="//storage.googleapis.com/vrview/2.0/index.html?image=images/alv.jpg&is_stereo=true" > </iframe>
					</div>
					<div class="col-lg-7">
						<p class="lead font">La realidad virtual(Vr) se podría definir como un sistema informático que genera en tiempo real representaciones de la realidad, que de hecho no son más que ilusiones ya que se trata de una realidad perceptiva sin ningún soporte físico y que únicamente se da en el interior de los ordenadores.<br><br>
						La realidad aumentada (RA) es el término que se usa para definir la visión de un entorno físico del mundo real, a través de un dispositivo tecnológico, es decir, los elementos físicos tangibles se combinan con elementos virtuales, logrando de esta manera crear una realidad aumentada en tiempo real. Consiste en un conjunto de dispositivos que añaden información virtual a la información física ya existente, es decir, añadir una parte sintética virtual a la real.
						</p>
					</div>
				</div>
			</div>
		</div>

		<!-- Contacto -->
		<div class="container-fluid barra" id="contacto">
			<div class="row">
				<div class="col-sm-3 col-md-4"></div>
				<div class="col-sm-6 col-md-4">
					<div id="alerta">
					</div>
					<form method="post">
						<h1 class="display-4 centrado font">Contactenos</h1>
						<small id="emailHelp" class="centrado  form-text text-muted">Nunca compartiremos tus datos con nadie.</small>
						<div class="form-group">
				   			<label for="email" class="font">Direccion email</label>
				    		<input name="email" type="email" class="form-control" id="email" placeholder="alguien@vrandarc.com">
			 			</div>
			 			<div class="form-row">
			 				<div class="col-6">
			 					<label for="name" class="font">Nombre</label>
			 					<input name="name_control" type="text" class="form-control" id="name_control" placeholder="Nombre">
			 				</div>
			 				<div class="col-6">
			 			 		<label for="number" class="font">Telefono(opcional)</label>
			 					<input name="number_contacto" type="tel" class="form-control" id="number_contacto" placeholder="33-4646-4646">
			 				</div>
			 			</div>
						<div class="form-group">
				    		<label for="empresa_contacto" class="font">Nombre de su empresa</label>
				    		<input name="empresa_contacto" type="text" class="form-control" id="empresa_contacto" placeholder="Vr&Arq">
			  			</div>
			  			<div class="form-group">
							<label for="contenido_contacto" class="font">Contenido</label>
							<textarea name="contenido_contacto" class="form-control" id="contenido_contacto" rows="5" placeholder="Texto"></textarea>
						</div>
			  			<div class="boton centrado">
			  				<button id="confim" type="submit" class="barra btn btn-primary">Enviar</button>
			  			</div>
					</form>
				</div>
			</div>
		</div>

	</body>
	<script type="text/javascript" src="js/letras.js"></script>
	<script type="text/javascript" src="js/formulario.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
