<nav class="fixed-top navbar navbar-expand-xl navbar-dark barra" id="navbar"> 
		 	<a class="navbar-brand" href="#">
		  		<img src="images/temporal.png" width="80" >
		  		Vr & Arq
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav mr-auto">
		      		<li class="nav-item">
		        		<a id="letra" class="nav-link" href="#quienesSomos">¿Quienes somos?</a>
		      		</li>
		      		<li class="nav-item">
		        		<a id="letra" class="nav-link" href="#queHacemos">¿Que hacemos?</a>
		      		</li>
		      		<li class="nav-item">
		        		<a id="letra" class="nav-link" href="#vrAr">¿Vr y Ar?</a>
		      		</li>
		      		<li class="nav-item">
		        		<a class="nav-link" href="#contacto">Contacto</a>
		      		</li>
		    	</ul>
		   		<form>
		    		<input type="email" name="email" class="formcontrol" placeholder="Email">
		    		<input type="password" name="password" class="formcontrol" placeholder="Password">
		    		<button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Login</button>
		    	</form>
		  	</div>
		</nav>